<?php
namespace Lubas\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

class DefaultBundleRepository extends EntityRepository
{
    protected function saveArrayWithTransaction($arraySave = [], $arraySoftDelete = [], $arrayHardDelete = [])
    {
        $em = $this->_em;
        $now = new \DateTime();
        $em->transactional(function () use($em, $arraySave, $arraySoftDelete, $arrayHardDelete, $now){
            foreach ($arraySave as $item){
                $em->persist($item);
            }
            foreach ($arraySoftDelete as $item) {
                $item->setDeletedAt($now);
                $em->persist($item);
            }
            foreach ($arrayHardDelete as $item) {
                $em->remove($item);
            }
            $em->flush();
        });
    }

    protected function save($item)
    {
        $em = $this->_em;
        $em->persist($item);
        $em->flush();
    }

    protected function delete($item, $isHardDelete=false)
    {
        $em = $this->_em;
        if($isHardDelete) {
            $em->remove($item);
        } else {
            $item->setDeletedAt(new \DateTime());
            $em->persist($item);
        }
        $em->flush();
    }

    protected function shuffleArrayKeys($arr)
    {
        $keys = array_keys($arr);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $arr[$key];
        }
        return $random;
    }
}