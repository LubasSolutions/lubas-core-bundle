<?php

namespace Lubas\CoreBundle\Service;

class ApiHandler {
    private static $allowedMethods = ['GET', 'POST', 'PUT', 'DELETE', 'HEAD', 'PATCH'];

    private $apiKey;
    public function __construct($apiKey = false)
    {
        $this->apiKey = $apiKey;
    }

    function doCall($method, $url, $data = false, $headers = false, $decodeResult = true){
        $curl = curl_init();

        if(!in_array($method, self::$allowedMethods)) {
            throw new \Exception('Method "'.$method.'" not allowed!');
        }

        if ($data) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        }

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeaders($headers));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result = curl_exec($curl);

        if(!$result) {
            throw new \Exception("Connection Failure");
        }

        curl_close($curl);
        return $decodeResult ? json_decode($result) : $result;
    }

    function doCallWithStatusCode($method, $url, $data = false, $headers = false, $decodeResult = true){
        $curl = curl_init();

        if(!in_array($method, self::$allowedMethods)) {
            throw new \Exception('Method "'.$method.'" not allowed!');
        }

        if ($data) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        }

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeaders($headers));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if(!$result) {
            throw new \Exception("Connection Failure");
        }

        curl_close($curl);

        return [$decodeResult ? json_decode($result) : $result, $httpcode];
    }

    private function getHeaders($headers)
    {
        if(empty($headers)) {
            $headers = [];
        }

        if($this->apiKey) {
            $headers[] = 'X-API-KEY: ' . $this->apiKey;
        }

        $headers[] = 'Content-Type: application/json';

        return $headers;
    }
}