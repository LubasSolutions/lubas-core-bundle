<?php

namespace Lubas\CoreBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CoreBundleController extends Controller
{
    protected $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    protected function getFileName($actualName, $type = 'sha512')
    {
        $now = new \DateTime();
        if($type == 'sha512') {
            return $now->format('Ymdhis') . $this->getRandomHash($type) . $this->getExtensionFromFileName($actualName);
        }
        if($type == 'md5') {
            return $now->format('Ymdhis').$this->getRandomHash($type).$this->getExtensionFromFileName($actualName);
        }
        if($type == 'uniqid') {
            return $now->format('Ymdhis') . $this->getRandomHash($type) . $this->getExtensionFromFileName($actualName);
        }
        return null;
    }

    private function getExtensionFromFileName($actualName)
    {
        $pieces = explode(".", $actualName);
        return '.' . end($pieces);
    }

    protected function getFormErrors($form)
    {
        $errors = [];
        if ($form->count() > 0) {
            foreach ($form->all() as $child) {
                if (!$child->isValid()) {
                    $config = $child->getConfig();
                    $label = $config->getOption("label");
                    $error = (String) $form[$child->getName()]->getErrors();
                    $error = str_replace('ERROR: ','',$error);
                    $errors[$label] = $error;
                }
            }
        }
        return $errors;
    }

    protected function setFileToEntity($item, $pathParameter, $oldFilename, $entityName = 'FileName', $fileName = null)
    {
        $fileX = $item->{'get'.$entityName}();
        if ($fileX != null) {
            if($fileName == null) {
                $filename = $this->getFileName($fileX->getClientOriginalName(), 'uniqid');
            } else {
                $filename = $fileName;
            }
            $fileX->move($pathParameter, $filename);

            if($oldFilename != null && $filename != $oldFilename) {
                if (file_exists($pathParameter . $oldFilename)) {
                    unlink($pathParameter . $oldFilename);
                }
            }

            $item->{'set'.$entityName}($filename);
        } else {
            $item->{'set'.$entityName}($oldFilename);
        }
    }

    protected function getFormAction($id, $isNew, $newPath, $editPath)
    {
        if($isNew) {
            return $this->generateUrl($newPath);
        }
        return $this->generateUrl($editPath, ['id' => $id]);
    }

    protected function getUnusualFormAction($isNew, $newPath, $editPath, $newParams = null, $editParams = null)
    {
        if($isNew) {
            if($newParams != null) {
                return $this->generateUrl($newPath, $newParams);
            }
            return $this->generateUrl($newPath);
        } else {
            if($editParams != null) {
                return $this->generateUrl($editPath, $editParams);
            }
            return $this->generateUrl($editPath);
        }
    }


    protected function save($entity)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
    }

    protected function delete($entity, $isHardDelete = false)
    {
        $em = $this->getDoctrine()->getManager();
        if($isHardDelete) {
            $em->remove($entity);
            $em->flush();
        } else {
            $entity->setDeletedAt(new \DateTime());
            $this->save($entity);
        }
    }

    protected function saveArrayWithTransaction($arraySave = [], $arraySoftDelete = [], $arrayHardDelete = [])
    {
        $em = $this->em;
        $now = new \DateTime();
        $em->transactional(function () use($em, $arraySave, $arraySoftDelete, $arrayHardDelete, $now){
            foreach ($arraySave as $item){
                $em->persist($item);
            }
            foreach ($arraySoftDelete as $item) {
                $item->setDeletedAt($now);
                $em->persist($item);
            }
            foreach ($arrayHardDelete as $item) {
                $em->remove($item);
            }
            $em->flush();
        });
    }

    protected function getGeneratedRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    protected function getRandomNumber($min = 1000, $max = 99999999)
    {
        return (int)rand((int)$min, (int)$max);
    }

    protected function getRandomHash($type = 'sha512')
    {
        switch ($type) {
            case 'sha512': return hash("sha256", ($this->getRandomNumber() * $this->getRandomNumber()).$this->getGeneratedRandomString());
            case 'md5': return md5(($this->getRandomNumber() * $this->getRandomNumber()).$this->getGeneratedRandomString());
            case 'uniqid': return uniqid();
            case 'password': return (substr(str_shuffle(('\\)/|@'.password_hash(mt_rand(0,999999), PASSWORD_DEFAULT).'!*^&~(')), 0, 100));
        }
        return null;
    }

    protected function removeDiacriticAndSpaces($text, $spaceReplace = '_')
    {
        $diac = array('ľ','š','č','ť','ž','ý','á','í','é','Č','Á','Ž','Ý','?','ô','!',' ');
        $cor = array('l','s','c','t','z','y','a','i' ,'e', 'C' ,'A', 'Z', 'Y','','o','',$spaceReplace);
        $text = str_replace($diac,$cor,$text);
        return $text;
    }

    protected function shuffleArrayKeys($arr)
    {
        $keys = array_keys($arr);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $arr[$key];
        }
        return $random;
    }

}
